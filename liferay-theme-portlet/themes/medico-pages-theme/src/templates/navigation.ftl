<nav class="${nav_css_class}" id="navigation" role="navigation">
	<h1 class="hide-accessible"><@liferay.language key="navigation" /></h1>
	
<div class="navbar-inner" style=" height: 93px">
	
	
	
	
<a class="brand" href="/home" style="margin-left:4px">
<!-- <img src="/o/vachans-theme/images/ecommerse.png"  style="float: left;
    margin-left: -18px;
    margin-top: -25px;
    height: 92px;
    width: 158px;" 
	alt="Amazon Logo " title="Amazon Logo" name="Amazon Logo" /> -->
	
	</a>
	<br>
	<img src="${images_folder}/ecommerse.png" style="float: left;
    margin-left: -18px;
    margin-top: -25px;
    height: 92px;
    width: 158px;"/>

<div class=" nav-collapse " id="navCollapse">
	<ul aria-label="<@liferay.language key="site-pages" />" role="menubar" class="navbar-nav nav list-style-none">
		<#list nav_items as nav_item>
			<#assign
				nav_item_attr_has_popup = ""
				nav_item_attr_selected = ""
				nav_item_css_class = ""
				nav_item_layout = nav_item.getLayout()
			/>

			<#if nav_item.isSelected()>
				<#assign
					nav_item_attr_has_popup = "aria-haspopup='true'"
					nav_item_attr_selected = "aria-selected='true'"
					nav_item_css_class = "selected"
				/>
			</#if>

			<li  data-toggle="dropdown"  ${nav_item_attr_selected} class="${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
				<a aria-labelledby="layout_${nav_item.getLayoutId()}" ${nav_item_attr_has_popup} href="${nav_item.getURL()}" ${nav_item.getTarget()} role="menuitem"><span><@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}</span>   <span class="caret"></span></a>

				<#if nav_item.hasChildren()>
					<ul class="child-menu dropdown-menu" role="menu">
						<#list nav_item.getChildren() as nav_child>
							<#assign
								nav_child_attr_selected = ""
								nav_child_css_class = ""
							/>

							<#if nav_item.isSelected()>
								<#assign
									nav_child_attr_selected = "aria-selected='true'"
									nav_child_css_class = "selected"
								/>
							</#if>
							
							<#--<li><a href="#">Page1</a></li>-->
         					 

							<li ${nav_child_attr_selected} class="${nav_child_css_class}" id="layout_${nav_child.getLayoutId()}" role="presentation">
								<a aria-labelledby="layout_${nav_child.getLayoutId()}" href="${nav_child.getURL()}" ${nav_child.getTarget()} role="menuitem">${nav_child.getName()}</a>
							</li>
						</#list>
					</ul>
				</#if>
			</li>
		</#list>
	</ul>
	

	<div>
	<ul class="mylogin nav navbar-nav navbar-right">
      <li style="margin-top: 12px;margin-right: 371px;">
      
      <#if !is_signed_in>
			
		
		 <a data-redirect="false" href="http://localhost:8080/c/portal/login?p_l_id=20146" id="sign-in" rel="nofollow">Sign In</a>
		
		<#else>
		
		<a href="/c/portal/logout" class="list-group-heading">Sign Out</a>
    
    	<#assign aDateTime = .now>
		<#assign aDate = aDateTime?date>
		<#assign aTime = aDateTime?time>

 
	<font color="white">Welcome, ${user_name} , ${aDateTime?iso_local}</font>

</#if>
      </li>
      <li></li>
      <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
      
    </ul>
    </div>
    </div>
</nav>


 <script>
	Liferay.Data.NAV_LIST_SELECTOR = '.navbar-inner .nav-collapse > ul';
</script> 