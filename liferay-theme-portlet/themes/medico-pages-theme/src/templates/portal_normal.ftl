<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div class="container-fluid" id="wrapper">
	<header id="banner" role="banner">
		<div id="heading">
			<h1 class="site-title">
				<a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
					<img alt="${logo_description}" height="${site_logo_height}" src="${site_logo}" width="${site_logo_width}" />
				</a>
				

				<#if show_site_name>
					<span class="site-name" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
						${site_name}
					</span>
				</#if>
			</h1>
		</div>

	<!--	<#if !is_signed_in>
			<a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}" id="sign-in" rel="nofollow">${sign_in_text}</a>
			helooooo out
		</#if> -->

		<#if has_navigation && is_setup_complete>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
	</header>

	<section id="content">
		<h1 class="hide-accessible">${the_title}</h1>

		<#-- <nav id="breadcrumbs">
			<@liferay.breadcrumbs />
		</nav>  -->

		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>

	<footer id="footer" class=footer-row role="contentinfo">
		<div class="container">
			<div class="footerSnippet">
				<div>
					<a class="w-inline-block" href="https://facebook.com/magento/" target="_blank">
					<img class="social-icon" src="https://info2.magento.com/rs/585-GGD-959/images/social-facebook.png">
					</a>
				</div>
				</br>
				<div class="copyright-etc">� 2017 Magento.
 					All Rights Reserved.
 					<a href="https://magento.com/legal/terms/privacy">Privacy Policy</a>
 					<a href="https://magento.com/legal/terms">Terms of service</a> 
 					<a href="https://magento.com/legal/licensing">License/Trademark FAQ</a>
 				</div>
			</div>
		</div>
		
		</br><p class="powered-by">
			<@liferay.language key="powered-by" /> <a href="http://www.liferay.com" rel="external">Anjali</a>
		</p>   
		
		 <#--<img src="/o/vachans-theme/images/download (3).jpg" alt="image1" width="300" height="200">-->
		<#-- <img src="/o/vachans-theme/images/download (2).jpg" alt="image" width="300" height="200">-->
		
		 
	</footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

<!-- inject:js -->
<!-- endinject -->

</body>

</html>