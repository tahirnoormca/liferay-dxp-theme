AUI().ready(
	'liferay-hudcrumbs', 'liferay-navigation-interaction', 'liferay-sign-in-modal',
	function(A) {
		var navigation = A.one('#navigation');

		var menu_toggle = navigation.one('#nav_toggle');

		var menu_1 =navigation.one('#menu');
		var subMenu_1 =navigation.one('#subMenu');
		 



		if (navigation) {
			navigation.plug(Liferay.NavigationInteraction);
		}

		menu_toggle.on('click', function(event) {
			navigation.one('.collapse.nav-collapse').toggleClass('open');



		});

		var siteBreadcrumbs = A.one('#breadcrumbs');

		if (siteBreadcrumbs) {
			siteBreadcrumbs.plug(A.Hudcrumbs);
		}

		var signIn = A.one('li.sign-in a');

		if (signIn && signIn.getData('redirect') !== 'true') {
			signIn.plug(Liferay.SignInModal);
		}
	}
	
);

